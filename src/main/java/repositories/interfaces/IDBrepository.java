package repositories.interfaces;

import java.sql.Connection;

public interface IDBrepository {
    Connection getConnection();
}
