package domain;

public class Owner {
    private int id = -1;
    private String name;
    private String surname;
    private int iin;
    private String password;


    private Owner() {

    }

    public Owner(String name, String surname, int iin, String password){
        setName(name);
        setSurname(surname);
        setIin(iin);
        setPassword(password);
    }

    public Owner(int id, String string, String name, int iin, String surname){
        setId(id);
        setName(name);
        setSurname(surname);
        setIin(this.iin);
        setPassword(password);
    }

    public int getIin() {
        return iin;
    }

    public void setIin(int iin) {
        this.iin = iin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName(){
        return name;
    }

    public String getSurname(){
        return surname;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSurname(String surName) {
        this.surname = surName;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname;
    }


}
