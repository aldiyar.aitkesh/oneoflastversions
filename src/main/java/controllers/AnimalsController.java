package controllers;

import domain.Animals;
import domain.Owner;
import services.AnimalsInteractor;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("animals")
public class AnimalsController {

    private AnimalsInteractor animalsInteractor;

    public AnimalsController() {
        animalsInteractor = new AnimalsInteractor();
    }

    @GET
    public String index() {
        return "hello Api";
    }

    @GET
    @Path("/{param}/owners/")
    public Response getAnimalsByOwnerID(@PathParam("param") int id) {
        Iterable<Owner> owners =  animalsInteractor.getAnimalsOwners(id);
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(owners)
                    .build();
    }

}
