package services;

import domain.Owner;
import repositories.entities.OwnerRepository;
import repositories.interfaces.IEntityRepository;

import java.util.LinkedList;

public class OwnerInteractor {
    private IEntityRepository ownerRepo;

    public OwnerInteractor() {
        ownerRepo =  new OwnerRepository(); }

    public Iterable<Owner> getOwnerByID(int id) {

        Iterable<Owner> list =  ownerRepo.query("SELECT * FROM owners WHERE id = " + id);
        return list;

    }

    public void addOwner(Owner owner){
        ownerRepo.add(owner);
    }

}
