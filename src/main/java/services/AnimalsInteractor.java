package services;

import domain.Animals;
import domain.Owner;
import repositories.entities.AnimalsRepository;
import repositories.entities.OwnerRepository;
import repositories.interfaces.IEntityRepository;

import java.util.LinkedList;

public class AnimalsInteractor {
    private IEntityRepository animalsRepo;
    private IEntityRepository ownerRepo;

    public AnimalsInteractor() {
        animalsRepo = new AnimalsRepository();
        ownerRepo = new OwnerRepository();
    }

    public Iterable<Owner> getAnimalsOwners(int id) {
         Iterable<Owner> list = ownerRepo.query("Select o.* ,a.*  from owners_animals_mapping m, owners o, animals a\n" +
                 "where m.id_owners = o.id and m.id_animals = a.id and o.id = " + id);
         return list;
    }

}
