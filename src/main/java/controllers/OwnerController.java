package controllers;

import domain.Owner;
import repositories.entities.OwnerRepository;
import services.OwnerInteractor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("owners")
public class OwnerController {

    private OwnerInteractor ownerInteractor;

    public OwnerController(){ ownerInteractor = new OwnerInteractor();}

    @GET
    public String hello(){
        return "hello";
    }

    @GET
    @Path("/{param}")
    public Response getOwnerByID(@QueryParam("Param") int id) {
        Iterable<Owner> owner = ownerInteractor.getOwnerByID(id);
        if (owner == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("User Doesn't exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(owner)
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Response CreateNewOwner(Owner owner)
    {
        ownerInteractor.addOwner(owner);
        return Response.status(Response.Status.CREATED)
                .entity("Owner Reated!")
                .build();
    }

}
