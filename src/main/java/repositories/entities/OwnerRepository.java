package repositories.entities;

import domain.Owner;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBrepository;
import repositories.interfaces.IEntityRepository;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class OwnerRepository implements IEntityRepository<Owner> {
        private IDBrepository dbrepo;

        public OwnerRepository() {
            dbrepo = new PostgresRepository();
        }


        @Override
        public void add(Owner entity) {
            try {
                String sql = "INCERT INTO owners(name, surname, iin, password)"+
                        "VALUES(?, ?, ?, ?)";

                PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
                stmt.setString(1, entity.getName());
                stmt.setString(2, entity.getSurname());
                stmt.setInt(3, entity.getIin());
                stmt.setString(4, entity.getPassword());
                stmt.execute();
            } catch (SQLException ex) {
                throw new ServerErrorException("Can not run SQL", 500);
            }
        }

        @Override
        public void update(Owner entity) {
                String sql = "UPDATE owners " +
                        "SET ";
                int c = 0;
                if (entity.getName() != null) {
                    sql += "name=?, "; c++;
                }
                if (entity.getSurname() != null) {
                    sql += "surname=?, "; c++;
                }

                if (entity.getPassword() != null) {
                    sql += "password=?, "; c++;
                }

                sql = sql.substring(0, sql.length() - 2);

                sql += " WHERE iin = ?";

                try {
                    int i = 1;
                    PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
                    if (entity.getName() != null) {
                        stmt.setString(i++, entity.getName());
                    }
                    if (entity.getSurname() != null) {
                        stmt.setString(i++, entity.getSurname());
                    }
                    if (entity.getPassword() != null) {
                        stmt.setString(i++, entity.getPassword());
                    }

                    stmt.setInt(i++, entity.getIin());

                    stmt.execute();
                } catch (SQLException ex) {
                    throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
                }
            }



        @Override
        public void remove(Owner owner) {

        }

        @Override
        public Iterable<Owner> query(String sql) {
            try {
                Statement stmt = dbrepo.getConnection().createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                LinkedList<Owner> owners = new LinkedList<>();
                while (rs.next()) {
                    Owner owner = new Owner(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getString("surname"),
                            rs.getInt("iin"),
                            rs.getString("password")
                    );
                    owners.add(owner);
                    }
                return owners;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return null;
        }
    }
